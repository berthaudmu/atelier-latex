# Classe Latex pour l'atelier v0.3b

## Description
Voici un magnifique classe Latex pour rédiger les documents à rendre en  atelier.

## Utilisation
La classe s'utilise comme n'importe quelle classe :
```latex
\documentclass{atelier}
```
Par défaut cela génèrera un template pour les rapports d'activité, pour pouvoir accéder aux autres templates, les options suivantes sont diponibles : 
 * rapportActivite
 * cahierDesCharges
 * rapportEssais
 * demandeEssais

Exemple : 
```latex
\documentclass[cahierDesCharges]{atelier}
```
La classe ajoute aussi deux commandes :
 * objet
 * contenu

Ces commandes doivent être utilisées dans le préambule.
La commande ```\objet```  prend un seul paramètre qui deviendra le contenu de la section *Objet* au début du document. De même la commande ```\contenu``` n'a qu'un paramètre qui deviendra le contenu de ce qui la "présentation du document" (le nom peut changer en fonction du template). Notons que cette commande n'a aucun effet quand elle est utilisée dans un *cahier des charges*, ce template n'ayant pas de présentation du document.

Un nouvel environnement est cependant disponible : ```exig```. Il offre la possibilité de mettre en forme les exigences du cahier des charge automatiquement à l'aide la commande ```\ex```. Son utilisation est similaire à l'environnement ```itemize```.

Exemple :
```latex
\begin{exig}
	\ex{Une signalisation de bon fonctionnement doit être présente.}{L’utilisateur doit pouvoir savoir en permanence et  sans ambiguïté si le système est opérationnel. }
	\ex{exigence}{commentaire}
\end{exig}
```
![Image de l'exemple ci-dessus](http://mr-z.org/f/ex.png)
## Instalation

Le fichier ```atelier.cls``` doit être placé dans le même répertoire que le document ```*.tex```.

## Versions 
### v0.1
 * Ajout de la classe altelier et de ses options
 * Ajout des commandes objet et contenu
 * Ajout du header
 
### v0.2
 * Ajout des formats de section/subsection/subsubsection personnalisé
 * Ajout d'un espace après le titre de présentation de contenu
 
### v0.3 
 * Ajout d'un environnement exig et de la commande \ex pour le cahier des charge, la command \ex doit etre utilisé dans l'environnement exig
 
### v0.3b
 * Correction du bug #1 : dépassement du texte dans les commandes objet et contenu
 * Ajout d'un saut de ligne après "Objet :"
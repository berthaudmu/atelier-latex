% Change Log : 
% v0.2  : - Ajout des formats de section/subsection/subsubsection 
%     personnalisé
%         - Ajout d'un espace après le titre de présentation de contenu
% v0.3  : - Ajout d'un environnement exig et de la commande \ex pour le 
%			cahier des charge, la command \ex doit etre utilisé dans 
%			l'environnement exig
% v0.3b : - Correction du bug #1 : dépassement du texte dans les 
%     commandes objet et contenu
%         - Ajout d'un saut de ligne après "Objet :"

\NeedsTeXFormat{LaTeX2e} %Version de latex requise (j'en sais trop rien, un peu mis au hasard)
\ProvidesClass{atelier}[2/12/2015 - v0.3b] %Propose la classe "atelier"

%Charge la classe "report" requise 
\LoadClass[a4paper]{report} 

%Packages requis
\RequirePackage[headheight=110pt,top=5cm, left=2cm, right=2cm, bottom=2cm]{geometry}
\RequirePackage{tabularx}
\RequirePackage{lastpage}
\RequirePackage{fancyhdr}
\RequirePackage[utf8]{inputenc}

%Condition de gestion si cachier des charges
\newif\if@notCdc\@notCdctrue
%Déclaration des options
\DeclareOption{rapportActivite}{
  \def\titre{Rapport d'activité}
  \def\contentTitle{Résumé / Conclusion}
}
\DeclareOption{cahierDesCharges}{
  \def\titre{Cahier des charges}
  \@notCdcfalse
}
\DeclareOption{rapportEssais}{
  \def\titre{Rapport d'essais}
  \def\contentTitle{Conclusion}
}
\DeclareOption{demandeEssais}{
  \def\titre{Demande d'essais}
  \def\contentTitle{Attendu}
}
%Par défaut on exécute l'option "rapportActivite"
\ExecuteOptions{rapportActivite}

%Fin de déclaration des options
\ProcessOptions\relax

\pagestyle{fancy}
\fancyhf{} %suppression des headers et footers actuels
\renewcommand{\headrulewidth}{0pt} %suppression de la ligne horizontale du header
\setlength{\fboxsep}{0pt} %Séparation (marge) des box de 0pt


%Récupération du contenu pour le tableau de presentation du rapport
\def\@obj{}
\def\objet#1{\def\@obj{#1}}
\def\@cont{}
\def\contenu#1{\def\@cont{#1}}

%Code éxécuter au début du document : header et tableau de présentation du contenu
\AtBeginDocument{
%Récupération des variable \date et \author
  \makeatletter
  \let\insertdate\@date
  \makeatother

  \makeatletter
  \let\insertauthor\@author
  \makeatother
  
%Déclaration du header
  \fancyhead[C]{
    \framebox[\textwidth][l]{
      \rule[-0.4cm]{0mm}{1cm}
      \makebox[.5\textwidth][c]{\Large{\titre}}
      \vrule
      \parbox{.5\textwidth}{
        \makebox[0.5\textwidth][l]{\rule[-0.4cm]{0mm}{1cm}\makebox[0.3\textwidth][l]{\hspace{0.3cm}Date : \insertdate}\vrule\makebox[0.2\textwidth][l]{\hspace{0.3cm}Page : \thepage/\pageref{LastPage}}}
        \rule{0.478\textwidth}{0.4pt}
        \makebox[0.5\textwidth][l]{\rule[-0.4cm]{0mm}{1cm}\hspace{0.3cm}Redacteur : \parbox[t]{\textwidth}{\insertauthor \\}}
      }
    }
  }
  %Tableau de résumé/objet et compagnie en début de rapport
  \noindent
  \if@notCdc{ %Si c'est pas le cahier des charges 
    \begin{tabularx}{\textwidth}{|X|}
      \hline \\
      \parbox{.97\textwidth}{
        Objet : \\
        \\
        \makeatletter
          \@obj 
           \\
        \makeatother
      } \\
      \hline\\
      \parbox{.97\textwidth}{
        \contentTitle\ : \\
        \\
        \makeatletter
          \@cont 
           \\
        \makeatother
      } \\
      \hline
    \end{tabularx}
  }
  \else{ %Si c'est le cahier des charges
    \begin{tabularx}{\textwidth}{|X|}
      \hline \\
      \parbox{.97\textwidth}{
        Objet : \\
        \makeatletter
          \@obj 
           \\
        \makeatother
      } \\
      \hline
    \end{tabularx}
  }
  \fi
  \ 
  
}
%Changement des sections...
\setcounter{secnumdepth}{3}
\def\thesection{\arabic{section}.}
\def\thesubsection{\thesection\arabic{subsection}}
\def\thesubsubsection{\thesubsection.\alph{subsubsection}}
%Environnement exigence cahier des charges
\newcounter{exCounter}
\newcommand\ex[2]{\item {} [ #1 ] #2}
\newenvironment{exig}{
  \setcounter{exCounter}{1}
  \begin{itemize} \renewcommand{\labelitemi}{Ex\_\theexCounter\stepcounter{exCounter} :}
}{
  \end{itemize}
}
%Fin de la déclaration de classe
\endinput
